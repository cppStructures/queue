#ifndef DQUEUE_H
    #define DQUEUE_H
#ifndef IQUEUE_H
    #include "iqueue.h"
#endif

#define NULL 0
#define nullptr 0

template <class ItemType>
class Queue: public IQueue<ItemType>{
    private:
        struct Node{
            ItemType value ;

            Node *next ;

            Node( void ) {
                value = NULL ;
                next = nullptr ;
            }
            ~Node( void ) {
                ;
            }

            private:
            // copy constructor & operator=
            Node( const Node &otherNode ) ;
            Node& operator= ( const Node &otherNode ) ;
        };

    Node *head, *tail ;

    size itemCount ;

    // helpers
    void emptyQueue( void ) ;

    // copy constructor & operator=
    Queue( const Queue &otherQueue ) ;
    Queue& operator= ( const Queue &otherQueue ) ;

    public:
        // Constructor & Destructor
        Queue( void ) ;
        ~Queue( void ) ;
        // Functions
        virtual ItemType pop( void ) ;
        virtual ItemType peak( void ) ;
        virtual void push( ItemType item ) ;
        virtual bool isEmpty( void ) ;
        virtual size qSize( void ) ;

} ;

//
// PRIVATE
//
template <class ItemType>
void Queue<ItemType>::emptyQueue( void ){
    while ( !isEmpty() ) {
        pop() ;
    }
}

//
// PUBLIC
//
template <class ItemType>
Queue<ItemType>::Queue( void ){
    head = nullptr ;
    tail = nullptr ;
    itemCount = 0 ;
}

template <class ItemType>
Queue<ItemType>::~Queue( void ){
    emptyQueue();
    if( tail ) delete tail ;
    if( head ) delete head ;
}

template <class ItemType>
ItemType Queue<ItemType>::pop( void ){
    ItemType tr = NULL ;
    if( !isEmpty() ){
        tr = head->value ;
        if( head != tail ){
            Node *newHead = head->next ;
            Node *oldHead = head ;
            head = newHead ;
            delete oldHead ;
        } else {
            Node *toDel = head ;
            head = nullptr ;
            tail = nullptr ;
            delete toDel ;
        }
        itemCount-- ;
    }
    return tr ;
}

template <class ItemType>
ItemType Queue<ItemType>::peak( void ){
    return head->value ;
}

template <class ItemType>
void Queue<ItemType>::push( ItemType item ){
    Node *newNode = new Node ;
    newNode->value = item ;

    if ( !isEmpty() ){
        Node *oldTail = tail ;
        oldTail->next = newNode ;
        tail = newNode ;
    } else {
        head = newNode ;
        tail = newNode ;
    }
    itemCount++ ;
}

template <class ItemType>
bool Queue<ItemType>::isEmpty( void ){
    return itemCount == 0 ;
}

template <class ItemType>
size Queue<ItemType>::qSize( void ){
    return itemCount ;
}

// factory
template <class ItemType>
IQueue<ItemType>* factoryQueue( void ) {
    return new Queue<ItemType> ;
}
#endif // DQUEUE_H
