#ifndef IQUEUE_H
#define IQUEUE_H

typedef unsigned long size ;

template <class ItemType>
class IQueue
{
    public:
        virtual ItemType pop( void ) = 0 ;
        virtual ItemType peak( void ) = 0 ;
        virtual void push( ItemType item ) = 0 ;
        virtual bool isEmpty( void ) = 0 ;
        virtual size qSize( void ) = 0 ;
};

#endif // IQUEUE_H
